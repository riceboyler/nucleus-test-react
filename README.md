This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

#Nucleus React Test

- Clone this repo and create a new branch with your name (i.e. JasonClark)
- Run `npm install` to install dependencies.
- Run `npm start` and it should load the main page with instructions.
- Follow the instructions on App.js. Essentially, you're building basic React components to display messages from a service.
- At the end of 30 minutes, please commit your changes to your local branch and you should be able to push that branch back to this repo.

##Notes
- You will need to be using at least Node v6 (prefer latest LTS) in order to get everything to work correctly. If needed `npm install -g nvm` and `nvm install 6` should get you where you need to be.