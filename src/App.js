import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Nucleus React Test</h2>
        </div>
        <p className="App-intro">
            First, a couple of handy notes:
            <ul>
                <li>The service to use for this exercise is at <a href="https://nucleus-test-service.herokuapp.com/api/messages" target="_blank">https://nucleus-test-service.herokuapp.com/api/messages</a>.
                </li>
                <li>The payload is returned in an object named "Payload".</li>
                <li>The shape of the message object is as follows:
                    <pre>
                        {"{"}<br />
                        {"    id: number,"}<br />
                        {"    title: string,"}<br />
                        {"    body: string,"}<br />
                        {"    sender: string,"}<br />
                        {"    recipient: string,"}<br />
                        {"    isRead: boolean,"}<br />
                        {"    sentDate: string (ISO)"}<br />
                        {"}"}
                    </pre>
                </li>
                <li>The object of this test is to have you show your React skills. Please create as many components as you like, pulling in whatever libraries you wish for fetching data, promises, formatting, etc.</li>
                <li>You will have 30 minutes to build a basic component to show the messages (and associated metadata) returned by the service. Bonus points will be awarded for "thinking in React", unit tests (Jest is built in) and aesthetic enhancement.</li>
                <li>To make your life easier, you can append the component(s) you create to this component, below the main paragraph tag.</li>
            </ul>
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
